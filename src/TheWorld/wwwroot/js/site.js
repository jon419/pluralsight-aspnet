// site.js
(function () {
    var $sidebarAndContent = $("#Sidebar, #Content");
    var $icon = $("#SidebarToggle i.fa");

    $("#SidebarToggle").on("click", function () {
        $sidebarAndContent.toggleClass("hide-sidebar");
        if ($sidebarAndContent.hasClass("hide-sidebar")) {
            $icon.removeClass("fa-angle-left");
            $icon.addClass("fa-angle-right");
        } else {
            $icon.removeClass("fa-angle-right");
            $icon.addClass("fa-angle-left");
        }
    });
    
})();

